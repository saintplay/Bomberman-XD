#pragma once
#include "Personaje.h"

class Upecino : public Personaje
{
	public:
		Upecino(void);
		~Upecino(void);

		void Mover(Direccion pDireccion);
		void Mostrar(Graphics^ pGraphics);
};

Upecino::Upecino(void)
{
	indiceSprites = 1;
	coordX = 200;
	coordY = 200;
	height = 50;
	width = 40;
	dx = 2;
	dy = 2;
}

Upecino::~Upecino(void)
{

}



void Upecino::Mover(Direccion pDireccion)
{
	if (pDireccion == Direccion::Abajo)
	{
		coordY += dy;
	}
	else if (pDireccion == Direccion::Izquierda)
	{
		coordX -= dx;
	}
	else if (pDireccion == Direccion::Arriba)
	{
		coordY -= dy;
	}
	else if (pDireccion == Direccion::Derecha)
	{
		coordX += dx;
	}
}

gcroot<Image^> imgBomberman = Image::FromFile("Recursos\\Bomberman.png");

Rectangle rAbajo1 = Rectangle(0,  0, 68, 100);
Rectangle rAbajo2 = Rectangle(68,  0, 68, 100);
Rectangle rAbajo3 = Rectangle(136, 0, 68, 100);
Rectangle rAbajo4 = Rectangle(204, 0, 68, 100);
Rectangle rIzquierda1 = Rectangle(0, 99, 68, 100);
Rectangle rIzquierda2 = Rectangle(68, 99, 68, 100);
Rectangle rIzquierda3 = Rectangle(136, 99, 68, 100);
Rectangle rIzquierda4 = Rectangle(204, 99, 68, 100);
Rectangle rArriba1 = Rectangle(0, 99, 68, 100);
Rectangle rArriba2 = Rectangle(68, 198, 68, 100);
Rectangle rArriba3 = Rectangle(136, 198, 68, 100);
Rectangle rArriba4 = Rectangle(204, 198, 68, 100);
Rectangle rDerecha1 = Rectangle(0, 297, 68, 100);
Rectangle rDerecha2 = Rectangle(68, 297, 68, 100);
Rectangle rDerecha3 = Rectangle(136, 297, 68, 100);
Rectangle rDerecha4 = Rectangle(204, 297, 68, 100);

void Upecino::Mostrar(Graphics^ pGraphics)
{
	Rectangle rectangle;

		 if (indiceSprites == 1) rectangle = rAbajo1;
		 else if (indiceSprites == 2) rectangle = rAbajo2;
		 else if (indiceSprites == 3) rectangle = rAbajo3;
		 else if (indiceSprites == 4) rectangle = rAbajo4;
		 else if (indiceSprites == 5) rectangle = rIzquierda1;
		 else if (indiceSprites == 6) rectangle = rIzquierda2;
		 else if (indiceSprites == 7) rectangle = rIzquierda3;
		 else if (indiceSprites == 8) rectangle = rIzquierda4;
		 else if (indiceSprites == 9) rectangle = rArriba1;
		 else if (indiceSprites == 10) rectangle = rArriba2;
		 else if (indiceSprites == 11) rectangle = rArriba3;
		 else if (indiceSprites == 12) rectangle = rArriba4;
		 else if (indiceSprites == 13) rectangle = rDerecha1;
		 else if (indiceSprites == 14) rectangle = rDerecha2;
		 else if (indiceSprites == 15) rectangle = rDerecha3;
		 else if (indiceSprites == 16) rectangle = rDerecha4;


	pGraphics->DrawImage(imgBomberman, Rectangle(coordX, coordY, 32, 32), rectangle, GraphicsUnit::Pixel);
	
}