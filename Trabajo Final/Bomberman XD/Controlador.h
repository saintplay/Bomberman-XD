#pragma once
#include "Upecino.h"

class Controlador
{
private:
	Personaje* jugador;

public:
	Controlador();
	~Controlador();

	void MoverPersonaje(Graphics^ graphics,BufferedGraphics^ buffer, Direccion pDireccion);
	void MostrarPersonaje(Graphics^ graphics);
};

Controlador::Controlador()
{
	jugador = new Upecino();
}

Controlador::~Controlador()
{
	delete jugador;
}

void Controlador::MoverPersonaje(Graphics^ graphics, BufferedGraphics^ buffer, Direccion pDireccion)
{

	if (pDireccion == Direccion::Abajo)
	{
		for (int i = 0; i < 3; i++)
		{
			buffer->Graphics->Clear(Color::SkyBlue);
			jugador->indiceSprites = 1;
			MostrarPersonaje(buffer->Graphics);
			jugador->Mover(pDireccion);
			buffer->Render(graphics);
			System::Threading::Thread::Sleep(100);

			buffer->Graphics->Clear(Color::SkyBlue);
			jugador->indiceSprites = 2;
			MostrarPersonaje(buffer->Graphics);
			jugador->Mover(pDireccion);
			buffer->Render(graphics);
			System::Threading::Thread::Sleep(100);

			buffer->Graphics->Clear(Color::SkyBlue);
			jugador->indiceSprites = 3;
			MostrarPersonaje(buffer->Graphics);
			jugador->Mover(pDireccion);
			buffer->Render(graphics);
			System::Threading::Thread::Sleep(100);
			
			buffer->Graphics->Clear(Color::SkyBlue);
			jugador->indiceSprites = 4;
			MostrarPersonaje(buffer->Graphics);
			jugador->Mover(pDireccion);
			buffer->Render(graphics);
			System::Threading::Thread::Sleep(100);		
		}
		buffer->Graphics->Clear(Color::SkyBlue);
		jugador->indiceSprites = 1;
		MostrarPersonaje(buffer->Graphics);
		buffer->Render(graphics);


	}
	else if (pDireccion == Direccion::Izquierda)
	{
		if (jugador->indiceSprites != 1)
			jugador->indiceSprites == 1;
		else if (jugador->indiceSprites == 1 || jugador->indiceSprites == 2 || jugador->indiceSprites == 3)
			jugador->indiceSprites++;
		else if (jugador->indiceSprites == 4)
			jugador->indiceSprites = 1;
	}
	else if (pDireccion == Direccion::Arriba)
	{
		if (jugador->indiceSprites != 1)
			jugador->indiceSprites == 1;
		else if (jugador->indiceSprites == 1 || jugador->indiceSprites == 2 || jugador->indiceSprites == 3)
			jugador->indiceSprites++;
		else if (jugador->indiceSprites == 4)
			jugador->indiceSprites = 1;
	}
	else if (pDireccion == Direccion::Derecha)
	{
		if (jugador->indiceSprites != 1)
			jugador->indiceSprites == 1;
		else if (jugador->indiceSprites == 1 || jugador->indiceSprites == 2 || jugador->indiceSprites == 3)
			jugador->indiceSprites++;
		else if (jugador->indiceSprites == 4)
			jugador->indiceSprites = 1;
	}
}


void Controlador::MostrarPersonaje(Graphics^ graphics)
{
	jugador->Mostrar(graphics);
}
