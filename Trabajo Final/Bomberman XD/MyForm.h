#pragma once
#include "Controlador.h"

namespace BombermanXD {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{

			controlador = new Controlador();
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		Graphics^ graphics;
		BufferedGraphics^ buffer;
		BufferedGraphicsContext^ context;
		Controlador* controlador;

		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->SuspendLayout();
			// 
			// MyForm
			// 
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::None;
			this->ClientSize = System::Drawing::Size(625, 416);
			this->Name = L"MyForm";
			this->Text = L"MyForm";
			this->Load += gcnew System::EventHandler(this, &MyForm::MyForm_Load);
			this->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MyForm::MyForm_KeyDown);
			this->ResumeLayout(false);

		}
#pragma endregion

	private: System::Void MyForm_Load(System::Object^  sender, System::EventArgs^  e)
	{
		graphics = this->CreateGraphics();
		context = BufferedGraphicsManager::Current;
		buffer = context->Allocate(graphics, this->ClientRectangle);
		buffer->Graphics->Clear(Color::White);
	}

	private: System::Void MyForm_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) 
	{
		buffer->Graphics->Clear(Color::SkyBlue);

		if (e->KeyCode == Keys::W)
			controlador->MoverPersonaje(graphics, buffer, Direccion::Arriba);
		else if (e->KeyCode == Keys::S)
			controlador->MoverPersonaje(graphics, buffer, Direccion::Abajo);
		else if (e->KeyCode == Keys::A)
			controlador->MoverPersonaje(graphics, buffer, Direccion::Izquierda);
		else if (e->KeyCode == Keys::D)
			controlador->MoverPersonaje(graphics, buffer, Direccion::Derecha);

		buffer->Render(graphics);
	}
	};
}
