#pragma once
#include <vcclr.h>

using namespace System::Drawing;

public enum Direccion {Arriba, Abajo, Izquierda, Derecha};
public enum Sprites {Derecha1, Derecha2, Derecha3, Derecha4, Izquierda1, Izquierda2, Izquierda3, Izquierda4, Abajo1, Abajo2, Abajo3, Abajo4, Arriba1, Arriba2, Arriba3, Arriba4};

class Personaje
{
protected:
	;
	int coordX;
	int coordY;
	int dx;
	int dy;
	int width;
	int height;
public:
	Personaje(void);
	~Personaje(void);

	int indiceSprites;

	void setCoordX(int pValor);
	void setCoordY(int pValor);
	void setDx(int pValor);
	void setDy(int pValor);
	void setWidth(int pValor);
	void setHeight(int pValor);

	int getIndice();
	int getCoordX();
	int getCoordY();
	int getDx();
	int getDy();
	int getWidth();
	int getHeight();

	virtual void Mover(Direccion pDireccion)abstract;
	virtual void Mostrar(Graphics^ pGraphics)abstract;
};

Personaje::Personaje(void)
{
	coordX = 0;
	coordY = 0;
	dx = 5;
	dy = 5;
	width = 0;
	height = 0;
}

Personaje::~Personaje(void)
{
}


void Personaje::setCoordX(int pValor){ coordX = pValor; }
void Personaje::setCoordY(int pValor){ coordY = pValor; }
void Personaje::setDx(int pValor){ dx = pValor; }
void Personaje::setDy(int pValor){ dy = pValor; }
void Personaje::setWidth(int pValor){ width = pValor; }
void Personaje::setHeight(int pValor){ height = pValor; }

int Personaje::getIndice(){ return indiceSprites; }
int Personaje::getCoordX(){	return coordX; }
int Personaje::getCoordY(){	return coordY; }
int Personaje::getDx(){ return dx; }
int Personaje::getDy(){ return dy; }
int Personaje::getWidth(){	return width; }
int Personaje::getHeight(){	return height; }